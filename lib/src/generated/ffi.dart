// AUTO GENERATED FILE, DO NOT EDIT.
//
// Generated by `package:ffigen`.
import 'dart:ffi' as ffi;

class NativeLibrary {
  /// Holds the symbol lookup function.
  final ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
      _lookup;

  /// The symbols are looked up in [dynamicLibrary].
  NativeLibrary(ffi.DynamicLibrary dynamicLibrary)
      : _lookup = dynamicLibrary.lookup;

  /// The symbols are looked up with [lookup].
  NativeLibrary.fromLookup(
      ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
          lookup)
      : _lookup = lookup;

  ffi.Pointer<BIO> BIO_new(
    ffi.Pointer<BIO_METHOD> type,
  ) {
    return _BIO_new(
      type,
    );
  }

  late final _BIO_newPtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<BIO> Function(ffi.Pointer<BIO_METHOD>)>>('BIO_new');
  late final _BIO_new = _BIO_newPtr.asFunction<
      ffi.Pointer<BIO> Function(ffi.Pointer<BIO_METHOD>)>();

  int BIO_read(
    ffi.Pointer<BIO> b,
    ffi.Pointer<ffi.Void> data,
    int dlen,
  ) {
    return _BIO_read(
      b,
      data,
      dlen,
    );
  }

  late final _BIO_readPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(
              ffi.Pointer<BIO>, ffi.Pointer<ffi.Void>, ffi.Int32)>>('BIO_read');
  late final _BIO_read = _BIO_readPtr.asFunction<
      int Function(ffi.Pointer<BIO>, ffi.Pointer<ffi.Void>, int)>();

  int BIO_write(
    ffi.Pointer<BIO> b,
    ffi.Pointer<ffi.Void> data,
    int dlen,
  ) {
    return _BIO_write(
      b,
      data,
      dlen,
    );
  }

  late final _BIO_writePtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(ffi.Pointer<BIO>, ffi.Pointer<ffi.Void>,
              ffi.Int32)>>('BIO_write');
  late final _BIO_write = _BIO_writePtr.asFunction<
      int Function(ffi.Pointer<BIO>, ffi.Pointer<ffi.Void>, int)>();

  int BIO_ctrl(
    ffi.Pointer<BIO> bp,
    int cmd,
    int larg,
    ffi.Pointer<ffi.Void> parg,
  ) {
    return _BIO_ctrl(
      bp,
      cmd,
      larg,
      parg,
    );
  }

  late final _BIO_ctrlPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int64 Function(ffi.Pointer<BIO>, ffi.Int32, ffi.Int64,
              ffi.Pointer<ffi.Void>)>>('BIO_ctrl');
  late final _BIO_ctrl = _BIO_ctrlPtr.asFunction<
      int Function(ffi.Pointer<BIO>, int, int, ffi.Pointer<ffi.Void>)>();

  ffi.Pointer<BIO_METHOD> BIO_s_mem() {
    return _BIO_s_mem();
  }

  late final _BIO_s_memPtr =
      _lookup<ffi.NativeFunction<ffi.Pointer<BIO_METHOD> Function()>>(
          'BIO_s_mem');
  late final _BIO_s_mem =
      _BIO_s_memPtr.asFunction<ffi.Pointer<BIO_METHOD> Function()>();

  int X509_STORE_add_cert(
    ffi.Pointer<X509_STORE> ctx,
    ffi.Pointer<X509> x,
  ) {
    return _X509_STORE_add_cert(
      ctx,
      x,
    );
  }

  late final _X509_STORE_add_certPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(ffi.Pointer<X509_STORE>,
              ffi.Pointer<X509>)>>('X509_STORE_add_cert');
  late final _X509_STORE_add_cert = _X509_STORE_add_certPtr.asFunction<
      int Function(ffi.Pointer<X509_STORE>, ffi.Pointer<X509>)>();

  int X509_VERIFY_PARAM_set1_host(
    ffi.Pointer<X509_VERIFY_PARAM> param,
    ffi.Pointer<ffi.Int8> name,
    int namelen,
  ) {
    return _X509_VERIFY_PARAM_set1_host(
      param,
      name,
      namelen,
    );
  }

  late final _X509_VERIFY_PARAM_set1_hostPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(
              ffi.Pointer<X509_VERIFY_PARAM>,
              ffi.Pointer<ffi.Int8>,
              ffi.Int32)>>('X509_VERIFY_PARAM_set1_host');
  late final _X509_VERIFY_PARAM_set1_host =
      _X509_VERIFY_PARAM_set1_hostPtr.asFunction<
          int Function(
              ffi.Pointer<X509_VERIFY_PARAM>, ffi.Pointer<ffi.Int8>, int)>();

  void X509_free(
    ffi.Pointer<X509> a,
  ) {
    return _X509_free(
      a,
    );
  }

  late final _X509_freePtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<X509>)>>(
          'X509_free');
  late final _X509_free =
      _X509_freePtr.asFunction<void Function(ffi.Pointer<X509>)>();

  ffi.Pointer<X509> d2i_X509(
    ffi.Pointer<ffi.Pointer<X509>> a,
    ffi.Pointer<ffi.Pointer<ffi.Uint8>> in1,
    int len,
  ) {
    return _d2i_X509(
      a,
      in1,
      len,
    );
  }

  late final _d2i_X509Ptr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<X509> Function(ffi.Pointer<ffi.Pointer<X509>>,
              ffi.Pointer<ffi.Pointer<ffi.Uint8>>, ffi.Int64)>>('d2i_X509');
  late final _d2i_X509 = _d2i_X509Ptr.asFunction<
      ffi.Pointer<X509> Function(ffi.Pointer<ffi.Pointer<X509>>,
          ffi.Pointer<ffi.Pointer<ffi.Uint8>>, int)>();

  int SSL_CTX_set_cipher_list(
    ffi.Pointer<SSL_CTX> arg0,
    ffi.Pointer<ffi.Int8> str,
  ) {
    return _SSL_CTX_set_cipher_list(
      arg0,
      str,
    );
  }

  late final _SSL_CTX_set_cipher_listPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(ffi.Pointer<SSL_CTX>,
              ffi.Pointer<ffi.Int8>)>>('SSL_CTX_set_cipher_list');
  late final _SSL_CTX_set_cipher_list = _SSL_CTX_set_cipher_listPtr.asFunction<
      int Function(ffi.Pointer<SSL_CTX>, ffi.Pointer<ffi.Int8>)>();

  ffi.Pointer<SSL_CTX> SSL_CTX_new(
    ffi.Pointer<SSL_METHOD> meth,
  ) {
    return _SSL_CTX_new(
      meth,
    );
  }

  late final _SSL_CTX_newPtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<SSL_CTX> Function(
              ffi.Pointer<SSL_METHOD>)>>('SSL_CTX_new');
  late final _SSL_CTX_new = _SSL_CTX_newPtr.asFunction<
      ffi.Pointer<SSL_CTX> Function(ffi.Pointer<SSL_METHOD>)>();

  void SSL_CTX_free(
    ffi.Pointer<SSL_CTX> arg0,
  ) {
    return _SSL_CTX_free(
      arg0,
    );
  }

  late final _SSL_CTX_freePtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<SSL_CTX>)>>(
          'SSL_CTX_free');
  late final _SSL_CTX_free =
      _SSL_CTX_freePtr.asFunction<void Function(ffi.Pointer<SSL_CTX>)>();

  ffi.Pointer<X509_STORE> SSL_CTX_get_cert_store(
    ffi.Pointer<SSL_CTX> arg0,
  ) {
    return _SSL_CTX_get_cert_store(
      arg0,
    );
  }

  late final _SSL_CTX_get_cert_storePtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<X509_STORE> Function(
              ffi.Pointer<SSL_CTX>)>>('SSL_CTX_get_cert_store');
  late final _SSL_CTX_get_cert_store = _SSL_CTX_get_cert_storePtr.asFunction<
      ffi.Pointer<X509_STORE> Function(ffi.Pointer<SSL_CTX>)>();

  void SSL_set_bio(
    ffi.Pointer<SSL> s,
    ffi.Pointer<BIO> rbio,
    ffi.Pointer<BIO> wbio,
  ) {
    return _SSL_set_bio(
      s,
      rbio,
      wbio,
    );
  }

  late final _SSL_set_bioPtr = _lookup<
      ffi.NativeFunction<
          ffi.Void Function(ffi.Pointer<SSL>, ffi.Pointer<BIO>,
              ffi.Pointer<BIO>)>>('SSL_set_bio');
  late final _SSL_set_bio = _SSL_set_bioPtr.asFunction<
      void Function(ffi.Pointer<SSL>, ffi.Pointer<BIO>, ffi.Pointer<BIO>)>();

  void SSL_CTX_set_verify(
    ffi.Pointer<SSL_CTX> ctx,
    int mode,
    SSL_verify_cb callback,
  ) {
    return _SSL_CTX_set_verify(
      ctx,
      mode,
      callback,
    );
  }

  late final _SSL_CTX_set_verifyPtr = _lookup<
      ffi.NativeFunction<
          ffi.Void Function(ffi.Pointer<SSL_CTX>, ffi.Int32,
              SSL_verify_cb)>>('SSL_CTX_set_verify');
  late final _SSL_CTX_set_verify = _SSL_CTX_set_verifyPtr.asFunction<
      void Function(ffi.Pointer<SSL_CTX>, int, SSL_verify_cb)>();

  ffi.Pointer<SSL> SSL_new(
    ffi.Pointer<SSL_CTX> ctx,
  ) {
    return _SSL_new(
      ctx,
    );
  }

  late final _SSL_newPtr = _lookup<
          ffi.NativeFunction<ffi.Pointer<SSL> Function(ffi.Pointer<SSL_CTX>)>>(
      'SSL_new');
  late final _SSL_new =
      _SSL_newPtr.asFunction<ffi.Pointer<SSL> Function(ffi.Pointer<SSL_CTX>)>();

  ffi.Pointer<X509_VERIFY_PARAM> SSL_get0_param(
    ffi.Pointer<SSL> ssl,
  ) {
    return _SSL_get0_param(
      ssl,
    );
  }

  late final _SSL_get0_paramPtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<X509_VERIFY_PARAM> Function(
              ffi.Pointer<SSL>)>>('SSL_get0_param');
  late final _SSL_get0_param = _SSL_get0_paramPtr.asFunction<
      ffi.Pointer<X509_VERIFY_PARAM> Function(ffi.Pointer<SSL>)>();

  void SSL_free(
    ffi.Pointer<SSL> ssl,
  ) {
    return _SSL_free(
      ssl,
    );
  }

  late final _SSL_freePtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<SSL>)>>(
          'SSL_free');
  late final _SSL_free =
      _SSL_freePtr.asFunction<void Function(ffi.Pointer<SSL>)>();

  int SSL_connect(
    ffi.Pointer<SSL> ssl,
  ) {
    return _SSL_connect(
      ssl,
    );
  }

  late final _SSL_connectPtr =
      _lookup<ffi.NativeFunction<ffi.Int32 Function(ffi.Pointer<SSL>)>>(
          'SSL_connect');
  late final _SSL_connect =
      _SSL_connectPtr.asFunction<int Function(ffi.Pointer<SSL>)>();

  int SSL_read(
    ffi.Pointer<SSL> ssl,
    ffi.Pointer<ffi.Void> buf,
    int num,
  ) {
    return _SSL_read(
      ssl,
      buf,
      num,
    );
  }

  late final _SSL_readPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(
              ffi.Pointer<SSL>, ffi.Pointer<ffi.Void>, ffi.Int32)>>('SSL_read');
  late final _SSL_read = _SSL_readPtr.asFunction<
      int Function(ffi.Pointer<SSL>, ffi.Pointer<ffi.Void>, int)>();

  int SSL_write(
    ffi.Pointer<SSL> ssl,
    ffi.Pointer<ffi.Void> buf,
    int num,
  ) {
    return _SSL_write(
      ssl,
      buf,
      num,
    );
  }

  late final _SSL_writePtr = _lookup<
      ffi.NativeFunction<
          ffi.Int32 Function(ffi.Pointer<SSL>, ffi.Pointer<ffi.Void>,
              ffi.Int32)>>('SSL_write');
  late final _SSL_write = _SSL_writePtr.asFunction<
      int Function(ffi.Pointer<SSL>, ffi.Pointer<ffi.Void>, int)>();

  int SSL_ctrl(
    ffi.Pointer<SSL> ssl,
    int cmd,
    int larg,
    ffi.Pointer<ffi.Void> parg,
  ) {
    return _SSL_ctrl(
      ssl,
      cmd,
      larg,
      parg,
    );
  }

  late final _SSL_ctrlPtr = _lookup<
      ffi.NativeFunction<
          ffi.Int64 Function(ffi.Pointer<SSL>, ffi.Int32, ffi.Int64,
              ffi.Pointer<ffi.Void>)>>('SSL_ctrl');
  late final _SSL_ctrl = _SSL_ctrlPtr.asFunction<
      int Function(ffi.Pointer<SSL>, int, int, ffi.Pointer<ffi.Void>)>();

  int SSL_get_error(
    ffi.Pointer<SSL> s,
    int ret_code,
  ) {
    return _SSL_get_error(
      s,
      ret_code,
    );
  }

  late final _SSL_get_errorPtr = _lookup<
          ffi.NativeFunction<ffi.Int32 Function(ffi.Pointer<SSL>, ffi.Int32)>>(
      'SSL_get_error');
  late final _SSL_get_error =
      _SSL_get_errorPtr.asFunction<int Function(ffi.Pointer<SSL>, int)>();

  ffi.Pointer<SSL_METHOD> DTLS_client_method() {
    return _DTLS_client_method();
  }

  late final _DTLS_client_methodPtr =
      _lookup<ffi.NativeFunction<ffi.Pointer<SSL_METHOD> Function()>>(
          'DTLS_client_method');
  late final _DTLS_client_method =
      _DTLS_client_methodPtr.asFunction<ffi.Pointer<SSL_METHOD> Function()>();

  int SSL_CTX_set_default_verify_paths(
    ffi.Pointer<SSL_CTX> ctx,
  ) {
    return _SSL_CTX_set_default_verify_paths(
      ctx,
    );
  }

  late final _SSL_CTX_set_default_verify_pathsPtr =
      _lookup<ffi.NativeFunction<ffi.Int32 Function(ffi.Pointer<SSL_CTX>)>>(
          'SSL_CTX_set_default_verify_paths');
  late final _SSL_CTX_set_default_verify_paths =
      _SSL_CTX_set_default_verify_pathsPtr.asFunction<
          int Function(ffi.Pointer<SSL_CTX>)>();

  int ERR_get_error() {
    return _ERR_get_error();
  }

  late final _ERR_get_errorPtr =
      _lookup<ffi.NativeFunction<ffi.Uint64 Function()>>('ERR_get_error');
  late final _ERR_get_error = _ERR_get_errorPtr.asFunction<int Function()>();

  ffi.Pointer<ffi.Int8> ERR_error_string(
    int e,
    ffi.Pointer<ffi.Int8> buf,
  ) {
    return _ERR_error_string(
      e,
      buf,
    );
  }

  late final _ERR_error_stringPtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<ffi.Int8> Function(
              ffi.Uint64, ffi.Pointer<ffi.Int8>)>>('ERR_error_string');
  late final _ERR_error_string = _ERR_error_stringPtr.asFunction<
      ffi.Pointer<ffi.Int8> Function(int, ffi.Pointer<ffi.Int8>)>();
}

class timeval extends ffi.Struct {
  @__time_t()
  external int tv_sec;

  @__suseconds_t()
  external int tv_usec;
}

typedef __time_t = ffi.Int64;
typedef __suseconds_t = ffi.Int64;
typedef BIO = bio_st;

class bio_st extends ffi.Opaque {}

typedef BIO_METHOD = bio_method_st;

class bio_method_st extends ffi.Opaque {}

typedef X509_STORE = x509_store_st;

class x509_store_st extends ffi.Opaque {}

typedef X509 = x509_st;

class x509_st extends ffi.Opaque {}

typedef X509_VERIFY_PARAM = X509_VERIFY_PARAM_st;

class X509_VERIFY_PARAM_st extends ffi.Opaque {}

typedef SSL_CTX = ssl_ctx_st;

class ssl_ctx_st extends ffi.Opaque {}

typedef SSL_METHOD = ssl_method_st;

class ssl_method_st extends ffi.Opaque {}

typedef SSL = ssl_st;

class ssl_st extends ffi.Opaque {}

typedef SSL_verify_cb = ffi.Pointer<
    ffi.NativeFunction<
        ffi.Int32 Function(ffi.Int32, ffi.Pointer<X509_STORE_CTX>)>>;
typedef X509_STORE_CTX = x509_store_ctx_st;

class x509_store_ctx_st extends ffi.Opaque {}

const int BIO_C_SET_BUF_MEM_EOF_RETURN = 130;

const int TLSEXT_NAMETYPE_host_name = 0;

const int SSL_VERIFY_NONE = 0;

const int SSL_VERIFY_PEER = 1;

const int SSL_ERROR_SSL = 1;

const int SSL_ERROR_ZERO_RETURN = 6;

const int SSL_CTRL_SET_TLSEXT_HOSTNAME = 55;

const int DTLS_CTRL_GET_TIMEOUT = 73;
